import json
import logging

import networkx as nx
import numpy as np
import pandas as pd
from jsoncomment import JsonComment

from .utils import (convert_string_to_date, match_drug, read_json_file,
                    save_csv_temp_file, save_graph_to_json)

logging.getLogger(__name__)


class GraphGen:
    '''
    This package contains the functions and helpers to
    prepare data and generate graph based on :
        publication data
        drug data
        clinical trial data
    '''

    def __init__(self):
        ''' Constructor '''

    def prepare_drug_data(self,
                          drug_dataset_input_filepath,
                          drug_dataset_output_filepath
                          ):
        ''' Process the drug dataset and prepare it for graph generation

        Parameters:
            drug_dataset_input_filepath : path to the drug csv dataset
            drug_dataset_output_filepath : path to the prepared output dataset

        Returns:
            None
        '''

        logging.info("Processing drug data.")

        # load
        drugs_df = pd.read_csv(drug_dataset_input_filepath)

        # No action done for this dataset
        # Add new preparation features
        # if necessary

        # save
        save_csv_temp_file(drugs_df, drug_dataset_output_filepath)

        return

    def prepare_publication_data(self,
                                 publication_csv_dataset_input_filepath,
                                 publication_json_dataset_input_filepath,
                                 publication_dataset_output_filepath,
                                 ):
        ''' Process the csv & json publication datasets
        Merges both and output a csv file

        Parameters :
            publication_csv_dataset_input_filepath :
                            path to the publication csv dataset
            publication_json_dataset_input_filepath :
                            path to the drug json dataset
            publication_dataset_output_filepath :
                            path to the prepared output dataset

        Returns:
            None
        '''

        logging.info("Processing publication data.")

        dtypes_dict = {
            "id": object,
            "title": str,
            "date": object,
            "journal": str
            }

        # Load CSV into dataframe
        publications_csv_df = pd.read_csv("../data/pubmed.csv",
                                          dtype=dtypes_dict,
                                          parse_dates=True
                                          )

        # Load JSON into dataframe
        # TODO: Work to avoid having corrupted json as input
        with open("../data/pubmed.json") as data_file:
            parser = JsonComment(json)
            publications_json_df = pd.DataFrame(parser.load(data_file))

        # replace json empty ids by Nan
        publications_json_df['id'].replace('', np.nan, inplace=True)
        publications_json_df.astype({'id': 'str'})

        # Drop invalid rows
        publications_json_df.dropna(inplace=True)

        # Merge both
        merged_publications_df = pd.concat([publications_csv_df,
                                            publications_json_df])

        # parse dates
        merged_publications_df['date'] = merged_publications_df['date'].apply(
                                            lambda x: convert_string_to_date(x)
                                                                             )

        # save
        save_csv_temp_file(merged_publications_df,
                           publication_dataset_output_filepath)

        return

    def prepare_clinical_trials_data(self,
                                     trials_dataset_input_filepath,
                                     trials_dataset_output_filepath,
                                     ):
        ''' Process the csv clinical trial dataset

        Parameters :
        trials_dataset_input_filepath :
                path to the clinical trials csv dataset
        trials_dataset_output_filepath :
                path to the prepared output dataset

        Returns:
            None
        '''

        logging.info("Processing clinical trials data.")

        # Load CSV into dataframe
        clinical_trials_df = pd.read_csv(
                        trials_dataset_input_filepath,
                        parse_dates=True)

        # Parse dates
        clinical_trials_df['date'] = clinical_trials_df['date'].apply(
                                    lambda x: convert_string_to_date(x)
                                    )

        # Check and drop NaN
        clinical_trials_df['scientific_title'] = \
            clinical_trials_df['scientific_title'].str.lstrip()

        clinical_trials_df['scientific_title'].replace('',
                                                       np.nan,
                                                       inplace=True)
        clinical_trials_df.dropna(inplace=True)

        # Save
        save_csv_temp_file(clinical_trials_df,
                           trials_dataset_output_filepath)

        return

    def generate_json_graph(self,
                            drug_dataset_input_filepath,
                            pubmed_dataset_input_filepath,
                            trials_dataset_input_filepath,
                            graph_json_output_filepath,
                            ):
        ''' Load the 3 input datasets and saves it as a JSON graph

        Parameters:

            drug_dataset_input_filepath : temporary csv file
            pubmed_dataset_input_filepath : temporary csv file
            trials_dataset_input_filepath : temporary csv file
            graph_json_output_filepath : path of the output json file

        Returns:
            None
        '''

        logging.info("Generating graph")

        # Load drug data
        drug_df = pd.read_csv(drug_dataset_input_filepath,
                              sep='|',
                              )
        # Load clinical trial data
        clinical_trials_df = pd.read_csv(trials_dataset_input_filepath,
                                         sep='|',
                                         )
        # Load publication data
        publications_df = pd.read_csv(
                                     pubmed_dataset_input_filepath,
                                     sep='|'
                                     )

        # get drug names
        drug_set = set(list(drug_df['drug']))

        # match drugs in clinical trial titles
        clinical_trials_df['drugs'] = \
            clinical_trials_df['scientific_title'].apply(
                lambda x: match_drug(x, drug_set)
                )

        # match drugs in publication titles
        publications_df['drugs'] = \
            publications_df['title'].apply(
                lambda x: match_drug(x, drug_set)
                )

        # create an empty  graph
        drug_graph = nx.Graph()

        # Populate graph with drug nodes
        for current_drug in drug_df.to_dict(orient='records'):

            drug_graph.add_node(current_drug['drug'],
                                type="drug",
                                atccode=current_drug['atccode']
                                )

        # Populate graph with trials and journal nodes
        for _, current_trial in clinical_trials_df.iterrows():

            drug_graph.add_node(current_trial['id'],
                                type="clinical_trial",
                                title=current_trial['scientific_title']
                                )

            drug_graph.add_node(current_trial['journal'],
                                type="journal")

        # Populate graph with publication and journal nodes
        for _, current_publication in publications_df.iterrows():

            drug_graph.add_node(current_publication['id'],
                                type="publication",
                                title=current_publication['title']
                                )

            drug_graph.add_node(current_publication['journal'],
                                type="journal"
                                )

        # Populate graph with trial edges
        for _, current_trial in clinical_trials_df.iterrows():

            for current_drug in current_trial['drugs']:

                if not drug_graph.has_edge(current_drug,
                                           current_trial['id']):

                    drug_graph.add_edge(current_drug,
                                        current_trial['id'],
                                        date_mention=[current_trial['date']]
                                        )

                    # link journal reference
                    drug_graph.add_edge(current_drug,
                                        current_trial['journal'],
                                        date_mention=[current_trial['date']]
                                        )

            # Populate graph with publication edges
            for _, current_publication in publications_df.iterrows():

                for current_drug in current_publication['drugs']:

                    if not drug_graph.has_edge(current_drug,
                                               current_publication['id']):
                        drug_graph.add_edge(current_drug,
                                            current_publication['id'],
                                            date_mention=[
                                                current_publication['date']
                                                         ]
                                            )

                    # link journal reference
                    drug_graph.add_edge(current_drug,
                                        current_publication['journal'],
                                        date_mention=[
                                                current_publication['date']
                                                     ]
                                        )

        logging.info("Generated graph")
        logging.info(nx.info(drug_graph))

        # save graph as json
        save_graph_to_json(drug_graph,
                           graph_json_output_filepath
                           )

        return

    def get_journal_max_count(self, json_graph_filename):
        ''' load a graph and returns the journals and their degrees
        sorted from max to min. It's easy to see the journal with
        the max distinct drugs.

        Parameters:
            json_graph_filename : json file

        Returns:
            string
        '''

        # read graph
        Graph = read_json_file(json_graph_filename)

        # get journal nodes
        journal_nodes = [x for x, y in Graph.nodes(data=True)
                         if y['type'] == 'journal']

        # Compute degrees for each nodes (1 drug count 1 degree)
        journal_degrees = dict(Graph.degree(journal_nodes))

        # sort the dict descending
        sort_journal_degrees = {k: v for k, v
                                in sorted(journal_degrees.items(),
                                          key=lambda item: item[1],
                                          reverse=True)
                                }

        return sort_journal_degrees
