import datetime
import json
import logging
import os
import re

from networkx.readwrite import json_graph

logging.getLogger(__name__)

# utility toolkit module


def save_csv_temp_file(dataframe, output_filepath):
    ''' Save a dataframe to a standard csv format

    Parameters:
        dataframe : pandas dataframe
        output_filepath : path of the saved file

    Returns:
        None
    '''

    dataframe.to_csv(output_filepath,
                     sep='|',
                     header=True,
                     index=False)
    return


def save_graph_to_json(graph, output_filepath):
    ''' Takes networkx graph and save it as json dump

    Parameters:
        graph : netxworkx graph
        output_filepath : path of the saved json

    Returns:
        None
    '''

    graph_json = json_graph.node_link_data(graph)
    graph_json_dump = json.dumps(graph_json)

    with open(output_filepath, 'w') as output_file:
        output_file.write(graph_json_dump)

    return


def convert_string_to_date(string_date):
    """ Detects the date format in the input string

    Parameters:
        string_date : a string representing a date

    Returns:
        datetime
    """
    if '-' in string_date:
        return datetime.datetime.strptime(string_date, '%Y-%m-%d')
    elif '/' in string_date:
        return datetime.datetime.strptime(string_date, '%d/%m/%Y')
    elif ' ' in string_date:
        return datetime.datetime.strptime(string_date, '%d %B %Y')


def match_drug(x, drug_set):
    ''' Find the potential matching drug terms with the verbatim

    Parameters:
        x : a text verbatim
        drug_set : a list of drugs

    Returns:
        list : list of matched drugs
    '''

    results = []

    for word in drug_set:
        if re.search(r'\b' + word + r'\b', x.upper()):
            results.append(word)

    return results


def read_json_file(filename):
    ''' read a graph in json format

    Parameters:
        filename : json filename

    Returns:
        graph : networkx format
    '''

    if os.path.exists(filename):
        with open(filename) as f:
            try:
                js_graph = json.load(f)
                node_link_graph = json_graph.node_link_graph(js_graph)

                return node_link_graph

            except ValueError:
                logging.error("Json read error")
