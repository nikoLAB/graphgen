# Python pipeline for drug oriented graph generation

GraphGen is a python package you can use for data preparation and json graph generation.

It can be used in a standalone module or inside a DAG.

The main data preparation steps are stored in the functions:

    GraphGen.prepare_drug_data()
    GraphGen.prepare_publication_data()
    GraphGen.prepare_clinical_trials_data()

The graph generation steps :

    GraphGen.generate_json_graph()

An additional function can be used to answer the ad-hoc treatment question

    GraphGen.get_journal_max_count()

Please see a demo of this package using the sample jupyter notebook included in the project.

## install

setup a virtual env

    python3 -m venv venv
    source venv/bin/activate

install requirements.txt

    pip install -r requirements.txt

If you want to run the package from jupyter with the demo jupyter notebook

    pip install jupyter

## Next steps in further release

To be implemented : Validate more toroughly the data and check potential discrepancies. Discuss with team how to deal with missing data

To be implemented : Better catch exceptions to make the pipeline robust to issues

To be implemented : Enable Installation of the package via setup.py for automatic deployment

    pip setup.py install

To be implemented : Run automatic lint & unit testing to check mainstream issues

    flake8
    pytest

## To go Further

If we want to handle higher volumes :

For data preparation :

- Manage data in object file storage such as S3 buckets.

- Switching to low mem data parsing (e.g. python generators) or switch to distributed compute (e.g. with pandas : python Dask, pyspark)

For graph data management :

- Switch to a real graph database such as Neo4J, Neptune, OrientDB, Dgraph ...

## SQL Test

### TEST PARTIE 1

~~~~sql
SELECT date, sum(prod_price*prod_qty) as ventes
FROM public."TRANSACTION"
WHERE date <= '2019-12-31' AND date >= '2019-01-01'
GROUP BY date
ORDER BY date asc
~~~~
--  /!\ dates without sales will be missing from the output

### TEST PARTIE 2

~~~~sql
SELECT 

	client_id,
	SUM(CASE WHEN product_type = 'MEUBLE' THEN ventes ELSE NULL END) AS ventes_meuble,
	SUM(CASE WHEN product_type = 'DECO' THEN ventes ELSE NULL END) AS ventes_deco
	
  FROM (
		SELECT client_id, product_type, sum(prod_price*prod_qty) as ventes
		FROM public."TRANSACTION" transactions
		JOIN public."PRODUCT_NOMENCLATURE" nomenclature
		ON transactions.prod_id = nomenclature.product_id
	  	WHERE date <= '2020-12-31' AND date >= '2020-01-01'

		GROUP BY transactions.client_id, nomenclature.product_type
	  ) subquery

GROUP BY client_id
~~~~